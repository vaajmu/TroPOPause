<?php

function resetMails() {
  global $mails;
  $mails = array(
                 "Return-Path: <fourmaux@l2ti.univ-paris13.fr>\nDelivered-To: online.fr-boiteres2004@free.fr\nReceived: (qmail 23137 invoked from network); 17 Oct 2004 19:17:21 -0000\nReceived: from galilee.univ-paris13.fr (HELO lpl.univ-paris13.fr) (194.254.164.38)\n  by mrelay3-1.free.fr with SMTP; 17 Oct 2004 19:17:21 -0000\nReceived: from pirogue.l2ti.univ-paris13.fr (pirogue.l2ti.univ-paris13.fr [194.254.163.182])\n.by lpl.univ-paris13.fr (8.12.10/8.12.10) with ESMTP id i9HJHL6X027011;\n.Sun, 17 Oct 2004 21:17:21 +0200 (CEST)\nReceived: from pirogue.l2ti.univ-paris13.fr (localhost [127.0.0.1])\n.by pirogue.l2ti.univ-paris13.fr (8.12.3/8.12.3/Debian-7.1) with ESMTP id i9HJHLkI014924;\n.Sun, 17 Oct 2004 21:17:21 +0200\nReceived: (from fourmaux@localhost)\n.by pirogue.l2ti.univ-paris13.fr (8.12.3/8.12.3/Debian-7.1) id i9HJHL9Y014922;\n.Sun, 17 Oct 2004 21:17:21 +0200\nFrom: Olivier Fourmaux <fourmaux@l2ti.univ-paris13.fr>\nDate: Sun, 17 Oct 2004 21:17:21 +0200\nTo: boiteres2004@free.fr\nSubject: Test\nMessage-ID: <20041017191721.GA14860@L2TI.univ-paris13.fr>\nMime-Version: 1.0\nContent-Type: multipart/mixed; boundary=\"pf9I7BMVVzbSWLtt\"\nContent-Disposition: inline\nUser-Agent: Mutt/1.3.28i\n\n\n--pf9I7BMVVzbSWLtt\nContent-Type: text/plain; charset=us-ascii\nContent-Disposition: inline\n\nCeci est un message au format MIME avec une image jointe. \n\n--pf9I7BMVVzbSWLtt\nContent-Type: image/png\nContent-Disposition: attachment; filename=\"openlogo-nd-50.png\"\nContent-Transfer-Encoding: base64\n\niVBORw0KGgoAAAANSUhEUgAAADIAAAA9CAMAAADYt8pWAAAAM1BMVEX////HADbLEEPjf5r7\n7/Lqn7Pxv83VQGj43+bcYIL0z9ntr8DOIE/SMFzgcI7mj6fZUHXA+KyXAAAAAWJLR0QAiAUd\nSAAAAAlwSFlzAAALEgAACxIB0t1+/AAAAAd0SU1FB9IBAhYHHEeg4/UAAAJKSURBVHiclVZJ\ngoQgDETCjiD/f+0kgDabOtapGymSygaMzRDSQ2QWlFh8XMFEvhF40Jpb/05TaRtgzbOFiUDQ\nnKs7xsFXjIywJLn9lkCAmSGbz0lHrxAedCNqjETD0K5ZF/IyHsQdw472YfnFXx6v8uDOsLR6\nzrVjFZeG84ubrSLkXa5Pzn4umEcTnecyi1NFH3cPDMZiY0Yyxd8ZTGTXeNlm/mGDlVBbwXOR\n2j4UFxymfzDDgZKgssVJuYASo6b06ehgUD/IlREMKraXcgp2ea7ljQKz6QMpERPjKgPp6w9F\nG5Ws4ofCFnvbU5dr+WxXzO09g+nNswXocOKnWbyfizkjVIqdxdttPSGo3TQrGerFiy0Ne48f\n5aiUfgNMbR57LTCph7V4tF63puJfR1mrN1gPURUHJ0q/YKqflMGcV+yD2FOOwdNYKZEqTpYf\ng1q3bV0nhPKPRqnxpjg2BmhfiqGJWMINkxakVDNCh4uMtqMXvlKGIKsYxsFYRoxjpUzgHBs/\n373no2tUvPo8Rk01plJETmjqjMYy/53r5naJwCV25dlkhip3b4OITobBcSZ5pEpPcIDNMyB2\n2tLcx2g74G13jnY+XpfH7FnxP0kJiGOeV2XyLYrXW60TLKs6322zb0+Qv5H+b9BlyJ8fBCPy\nXRHf9zUQ+2KWvUB+F3NNqA+gSbC6Yp4gP6em3jevl18HEVZPkX9wPsohDk/f9GQ705x44eSO\n/Rg3ajdtvlWowzrg+8faye/C9QXzQEJL9ltWseQOzXcNN4/3P3OaDmqvPEXSAAAAAElFTkSu\nQmCC\n\n--pf9I7BMVVzbSWLtt--\n",
                 "Return-Path: <steve@example.com>\nX-Original-To: john@example.com\nDelivered-To: john@example.com\nReceived: from example.com (localhost [127.0.0.1])\n    by ... (Postfix) with ESMTP id 692DF379C7\n    for <john@example.com>; Fri, 18 May 2007 22:59:31 +0200 (CEST)\nMessage-Id: <...>\nDate: Fri, 18 May 2007 22:59:31 +0200 (CEST)\nFrom: steve@example.com\nTo: undisclosed-recipients:;\n\nHi John,\n\njust wanted to drop you a note.",
                 "x-store-info:i1mvqhPkdZwu3DNZ/OabHRDR0K9F4ru4KWlzuzv6VF28qgXI7UUJSsSXlrJfozcPp2dbj8rY8JRgtd7HDMrM1zuDa6kWz293eMpjKrzQWRghwJSZHDjfmCR244RFafvp8Z27KVvVt8n0SwRzRGtUF/OZRZHFsCla\nAuthentication-Results: hotmail.com; spf=pass (sender IP is 66.220.144.155; identity alignment result is pass and alignment mode is relaxed) smtp.mailfrom=notification+i-pphkhm@facebookmail.com; dkim=pass (identity alignment result is pass and alignment mode is relaxed) header.d=facebookmail.com; x-hmca=pass header.id=notification+i-pphkhm@facebookmail.com\nX-SID-PRA: notification+i-pphkhm@facebookmail.com\nX-AUTH-Result: PASS\nX-SID-Result: PASS\nX-Message-Status: n:n\nX-Message-Delivery: Vj0xLjE7dXM9MTtsPTE7YT0xO0Q9MTtHRD0xO1NDTD0w\nX-Message-Info: /Afko6AgMSwAiQ52xyLpA4Ffh+8zPEJm5qM7UO8i+Ml7NAwVH8P1P3bnMWxbi0uYjm1cY74o77qzdMM9f62838zpk2YTEuJVe/snUTYYHn+mRXqhoNb4Q3pvr5Cz6k/gIv3nPtwYqylS8SbXeIyQM/OIvTZyXDuwYPza1LWSb8eetRQxme5LFKBLsZEY//3xWIhJ8nIIo7f3ruaYY315zcK/dpyCA9PG\nReceived: from mx-out.facebook.com ([66.220.144.155]) by BAY004-MC6F10.hotmail.com over TLS secured channel with Microsoft SMTPSVC(7.5.7601.23008);\n	 Wed, 22 Apr 2015 10:13:18 -0700\nDKIM-Signature: v=1; a=rsa-sha256; c=relaxed/simple; d=facebookmail.com;\n	s=s1024-2013-q3; t=1429722798;\n	bh=U7NV//xBAaJrCqx0CQeGT3gzkIlyWyb5/yJfajyjSyg=;\n	h=Date:To:From:Subject:MIME-Version:Content-Type;\n	b=N9rse1atpS2S1iUlYd9hOm4E1mY0BOYVwFW6pzLxuI+rmzZcuUF5quZOQTWaIPyRP\n	 aMl9J3+BtyzSb10RpMPvuo6S03PiUsI+NiwcjFJc5V1RuXcFSda5TX3XjOU3H3Dil2\n	 1jIJ3+yghnCMG5uStOOyASZZOjRIdrHP/XbROL2U=\nReceived: from facebook.com (Id27UM4z7vldvVZbo8D6Jy6kloVWp4uy7PX0NADte30jX9Cr08e9+FDPSSgooQmy 10.224.41.49)\n by facebook.com with Thrift id df0f8e1ee91211e4a3f60002c99308fa-698f2310;\n Wed, 22 Apr 2015 10:13:18 -0700\nX-Facebook: from 10.210.136.43 ([MTI3LjAuMC4x]) \n	by async.facebook.com with HTTP (ZuckMail);\nDate: Wed, 22 Apr 2015 10:13:18 -0700\nReturn-Path: notification+i-pphkhm@facebookmail.com\nTo: Antoine Loheac <orlin91@hotmail.fr>\nFrom: \"Facebook\" <notification+i-pphkhm@facebookmail.com>\nReply-to: noreply <noreply@facebookmail.com>\nSubject: =?UTF-8?B?Tm91dmVhdXggbWVzc2Fn?=\n =?UTF-8?B?ZXMgZGUgUGllcnJlIE1h?=\n =?UTF-8?B?aMOp?=\nX-Priority: 3\nX-Mailer: ZuckMail [version 1.00]\nErrors-To: notification+i-pphkhm@facebookmail.com\nX-Facebook-Notify: msg; from=1526166796; t=mid.1424711490404:cb2cabde4ae5be5440; mailid=ba1d3dbG629f398bG0G0G7293e656\nX-FACEBOOK-PRIORITY: 0\nX-Auto-Response-Suppress: All\nMessage-ID: <3053e4851a78ab52d0a37274e9f248c6@async.facebook.com>\nMIME-Version: 1.0\nContent-Type: multipart/alternative;	boundary=\"b1_3053e4851a78ab52d0a37274e9f248c6\"\nX-OriginalArrivalTime: 22 Apr 2015 17:13:18.0996 (UTC) FILETIME=[A0DBC540:01D07D1F]\n\n\n--b1_3053e4851a78ab52d0a37274e9f248c6\nContent-Type: text/plain; charset=\"UTF-8\"\nContent-Transfer-Encoding: quoted-printable\n\nPierre vous a envoy=C3=A9 un message.\n\n\n\n\"?\"\n\nPour r=C3=A9pondre =C3=A0 ce message, cliquez sur le lien suivant=C2=A0:\nhttps://www.facebook.com/n/?messages%2F&action=3Dread&tid=3Dmid.1424711490=\n404%3Acb2cabde4ae5be5440&medium=3Demail&mid=3Dba1d3dbG629f398bG0G0G7293e65=\n6&n_m=3Dorlin91%40hotmail.fr\n\nRetrouvez des contacts de votre carnet d=E2=80=99adresses Outlook.com =\n(Hotmail) sur Facebook=C2=A0:\nhttps://www.facebook.com/n/?find-friends%2F&ref=3Demail&medium=3Demail&mid=\n=3Dba1d3dbG629f398bG0G0G7293e656&n_m=3Dorlin91%40hotmail.fr\n\n\n=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=\n=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D=3D\nCe message a =C3=A9t=C3=A9 envoy=C3=A9 =C3=A0 orlin91@hotmail.fr. Si vous =\nne souhaitez plus recevoir ces messages de la part de Facebook, veuillez =\nsuivre le lien ci-dessous pour annuler votre abonnement.\nhttps://www.facebook.com/o.php?k=3DAS3UNUzQS77c1deS&u=3D1654602123&mid=3Db=\na1d3dbG629f398bG0G0G7293e656\nFacebook, Inc., Attention: Department 415, PO Box 10005, Palo Alto, CA =\n94303\n\n\n--b1_3053e4851a78ab52d0a37274e9f248c6\nContent-Type: text/html; charset=\"UTF-8\"\nContent-Transfer-Encoding: quoted-printable\n\n<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional //EN\">\n<html><head><meta http-equiv=3D\"Content-Type\" content=3D\"text/html; =\ncharset=3Dutf-8\"><style type=3D\"text/css\">a.thumb:link, a.thumb:visited =\n{border:1px #CCCCCC solid !important;}a.thumb:active, a.thumb:hover =\n{border:1px #3B5998 solid !important;}.uiScaledImageContainer {position: =\nrelative;overflow: hidden;}</style><title>Facebook</title></head><body =\nstyle=3D\"margin:0;padding:0;\" dir=3D\"ltr\"><table width=3D\"98%\" =\nborder=3D\"0\" cellspacing=3D\"0\" cellpadding=3D\"8\"><tr><td =\nbgcolor=3D\"#FFFFFF\" width=3D\"100%\" style=3D\"font-family:'lucida =\ngrande',tahoma,verdana,arial,sans-serif;\"><table cellpadding=3D\"0\" =\ncellspacing=3D\"0\" border=3D\"0\" width=3D\"500\"><tr><td colspan=3D\"2\" =\nstyle=3D\"padding:10px 0 0 =\n10px;color:#000000;font-size:13px;font-family:'lucida =\ngrande',tahoma,verdana,arial,sans-serif;\" valign=3D\"top\"><table =\nwidth=3D\"100%\" style=3D\"color:#000000;font-size:13px;\"><tr><td =\nwidth=3D\"100%\" valign=3D\"top\" align=3D\"left\" style=3D\"font-family:'lucida =\ngrande',tahoma,verdana,arial,sans-serif;color:#000000;font-size:13px;\"><di=\nv style=3D\"margin-bottom:15px;\"><table cellpadding=3D\"0\" cellspacing=3D\"0\" =\nstyle=3D\"width:100%;padding-top:7px;\"><tr><td valign=3D\"top\" =\nstyle=3D\"padding:3px 5px 5px 0px;width:57px;\"><a href=3D\"https://www.faceb=\nook.com/n/?pierre.mahe.trp&amp;medium=3Demail&amp;mid=3Dba1d3dbG629f398bG0=\nG0G7293e656&amp;n_m=3Dorlin91%40hotmail.fr\" =\nstyle=3D\"color:#3b5998;text-decoration:none;\"><img src=3D\"https://scontent=\n.xx.fbcdn.net/hprofile-xfa1/v/l/t1.0-1/c8.0.50.50/p50x50/10329124_10204712=\n527362837_3432709011123321469_n.jpg?oh=3D9b5a175f10df38aaf224840a63f20dd2&=\namp;oe=3D55DEE16C\" alt=3D\"Pierre Mah&#xe9;\" style=3D\"border: 0; =\nheight:50px; width:50px; \" /></a></td><td valign=3D\"top\" align=3D\"left\" =\nstyle=3D\"padding:5px 5px 5px 0;font-family:&#039;lucida =\ngrande&#039;,tahoma,verdana,arial,sans-serif;\"><table cellpadding=3D\"0\" =\ncellspacing=3D\"0\" style=3D\"width:100%;padding-bottom:5px;\"><tr><td =\nstyle=3D\"font-family:&#039;lucida grande&#039;,tahoma,verdana,arial,sans-s=\nerif;color:#000000;font-size:13px;\"><a =\nstyle=3D\"color:#3b5998;text-decoration:none;font-weight:bold;\" =\nhref=3D\"https://www.facebook.com/n/?pierre.mahe.trp&amp;medium=3Demail&amp=\n;mid=3Dba1d3dbG629f398bG0G0G7293e656&amp;n_m=3Dorlin91%40hotmail.fr\">Pierr=\ne Mah=C3=A9</a></td><td style=3D\"text-align:right;color:#999999;padding-ri=\nght:5px;font-family:&#039;lucida =\ngrande&#039;,tahoma,verdana,arial,sans-serif;font-size:11px;\"> 22 avril =\n17:30 </td></tr></table><div style=3D\"width:458px;word-wrap:break-word;pad=\nding-bottom:7px;color:#000000;font-size:13px;\">la premiere elle est si =\npuissante</div></td></tr></table><table cellpadding=3D\"0\" =\ncellspacing=3D\"0\" style=3D\"width:100%;padding-top:7px;\"><tr><td =\nvalign=3D\"top\" style=3D\"padding:3px 5px 5px 0px;width:57px;\"><a =\nhref=3D\"https://www.facebook.com/n/?pierre.mahe.trp&amp;medium=3Demail&amp=\n;mid=3Dba1d3dbG629f398bG0G0G7293e656&amp;n_m=3Dorlin91%40hotmail.fr\" =\nstyle=3D\"color:#3b5998;text-decoration:none;\"><img src=3D\"https://scontent=\n.xx.fbcdn.net/hprofile-xfa1/v/l/t1.0-1/c8.0.50.50/p50x50/10329124_10204712=\n527362837_3432709011123321469_n.jpg?oh=3D9b5a175f10df38aaf224840a63f20dd2&=\namp;oe=3D55DEE16C\" alt=3D\"Pierre Mah&#xe9;\" style=3D\"border: 0; =\nheight:50px; width:50px; \" /></a></td><td valign=3D\"top\" align=3D\"left\" =\nstyle=3D\"padding:5px 5px 5px 0;font-family:&#039;lucida =\ngrande&#039;,tahoma,verdana,arial,sans-serif;\"><table cellpadding=3D\"0\" =\ncellspacing=3D\"0\" style=3D\"width:100%;padding-bottom:5px;\"><tr><td =\nstyle=3D\"font-family:&#039;lucida grande&#039;,tahoma,verdana,arial,sans-s=\nerif;color:#000000;font-size:13px;\"><a =\nstyle=3D\"color:#3b5998;text-decoration:none;font-weight:bold;\" =\nhref=3D\"https://www.facebook.com/n/?pierre.mahe.trp&amp;medium=3Demail&amp=\n;mid=3Dba1d3dbG629f398bG0G0G7293e656&amp;n_m=3Dorlin91%40hotmail.fr\">Pierr=\ne Mah=C3=A9</a></td><td style=3D\"text-align:right;color:#999999;padding-ri=\nght:5px;font-family:&#039;lucida =\ngrande&#039;,tahoma,verdana,arial,sans-serif;font-size:11px;\"> 22 avril =\n18:43 </td></tr></table><div style=3D\"width:458px;word-wrap:break-word;pad=\nding-bottom:7px;color:#000000;font-size:13px;\">vendredi apres midi on fait =\nacii</div></td></tr></table><table cellpadding=3D\"0\" cellspacing=3D\"0\" =\nstyle=3D\"width:100%;padding-top:7px;\"><tr><td valign=3D\"top\" =\nstyle=3D\"padding:3px 5px 5px 0px;width:57px;\"><a =\nhref=3D\"https://www.facebook.com/pierre.mahe.trp\" =\nstyle=3D\"color:#3b5998;text-decoration:none;\"><img src=3D\"https://scontent=\n.xx.fbcdn.net/hprofile-xfa1/v/l/t1.0-1/c8.0.50.50/p50x50/10329124_10204712=\n527362837_3432709011123321469_n.jpg?oh=3D9b5a175f10df38aaf224840a63f20dd2&=\namp;oe=3D55DEE16C\" alt=3D\"Pierre Mah&#xe9;\" style=3D\"border: 0; =\nheight:50px; width:50px; \" /></a></td><td valign=3D\"top\" align=3D\"left\" =\nstyle=3D\"padding:5px 5px 5px 0;font-family:&#039;lucida =\ngrande&#039;,tahoma,verdana,arial,sans-serif;\"><table cellpadding=3D\"0\" =\ncellspacing=3D\"0\" style=3D\"width:100%;padding-bottom:5px;\"><tr><td =\nstyle=3D\"font-family:&#039;lucida grande&#039;,tahoma,verdana,arial,sans-s=\nerif;color:#000000;font-size:13px;\"><a =\nstyle=3D\"color:#3b5998;text-decoration:none;font-weight:bold;\" =\nhref=3D\"https://www.facebook.com/pierre.mahe.trp\">Pierre =\nMah=C3=A9</a></td><td style=3D\"text-align:right;color:#999999;padding-righ=\nt:5px;font-family:&#039;lucida =\ngrande&#039;,tahoma,verdana,arial,sans-serif;font-size:11px;\"> 22 avril =\n18:43 </td></tr></table><div style=3D\"width:458px;word-wrap:break-word;pad=\nding-bottom:7px;color:#000000;font-size:13px;\">?</div></td></tr></table></=\ndiv></td></tr></table><span style=3D\"\"><img src=3D\"https://www.facebook.co=\nm/email_open_log_pic.php?mid=3Dba1d3dbG629f398bG0G0G7293e656\" =\nstyle=3D\"border:0;width:1px;height:1px;\" /></span><br /></td></tr><tr><td =\ncolspan=3D\"2\" style=3D\"color:#666666;padding:10px 5px 15px =\n10px;border-top:#E9E9E9 1px solid;line-height: 18px;font-size: =\n12px;font-family:'lucida grande',tahoma,verdana,arial,sans-serif;\"><a =\nhref=3D\"https://www.facebook.com/n/?messages%2F&amp;action=3Dread&amp;tid=\n=3Dmid.1424711490404%3Acb2cabde4ae5be5440&amp;medium=3Demail&amp;mid=3Dba1=\nd3dbG629f398bG0G0G7293e656&amp;n_m=3Dorlin91%40hotmail.fr\" =\nstyle=3D\"color:#3b5998; text-decoration:none;\">Afficher la conversation  =\nsur Facebook</a><br>Ce message a =C3=A9t=C3=A9 envoy=C3=A9 =C3=A0 <a =\nhref=3D\"mailto:orlin91&#064;hotmail.fr\" =\nstyle=3D\"color:#3b5998;text-decoration:none;\">orlin91&#064;hotmail.fr</a>. =\nSi vous ne souhaitez plus recevoir ces messages de la part de Facebook, =\nveuillez <a href=3D\"https://www.facebook.com/o.php?k=3DAS3UNUzQS77c1deS&am=\np;u=3D1654602123&amp;mid=3Dba1d3dbG629f398bG0G0G7293e656\" =\nstyle=3D\"color:#3b5998;text-decoration:none;\">vous d=C3=A9sabonner</a>.<br =\n/>Facebook, Inc., Attention: Department 415, PO Box 10005, Palo Alto, CA =\n94303</td></tr></table></td></tr></table></body></html>\n\n\n\n--b1_3053e4851a78ab52d0a37274e9f248c6--\n");
  return "+OK ";
}

function traiterUser($str) {
  global $user, $connecte;

  /* echo "traiterUser --".trim($str)."--\n"; */

  if($connecte) return "+ERR Already connected";

  $pseudo = trim($str);
  if($pseudo) {
    $user = $pseudo;
    return "+OK ";
  } else {
    return "+ERR empty";
  }
}

function traiterPass($str) {
  global $membres, $connecte, $user;
  
  /* echo "traiterPass --".$str."--\n"; */

  if(!$user) return "+ERR USER first";
  if($connecte) return "+ERR Already connected";

  if(isset($membres[$user]) && $membres[$user] === $str) {
    $connecte = true;
    return "+OK ";
  } else {
    $user = "";
    $connecte = false;
    return "+ERR Connection failed";
  }
}

function traiterList() {
  global $mails, $connecte;
  $reponse = "+OK \n";

  /* echo "traiterList ----\n"; */

  if(!$connecte) return "+ERR Authorization first";

  foreach($mails as $ind => $m) {
    $reponse .= "$ind " . strlen($m) . "\n";
  }

  return $reponse . ".";
}

function traiterQuit() {
  global $user, $connecte;
  
  /* echo "traiterQuit ----\n"; */

  /* if(!$connecte) return "+ERR Authorization first"; */

  $user = "";
  $connecte = "";

  return "+OK ";
}

function traiterRetr($arg) {
  global $user, $connecte, $mails;
  
  /* echo "traiterRetr --".$arg."--\n"; */
  
  if(!$connecte) return "+ERR Authorization first";

  $n = intval($arg);

  if(isset($mails[$n])) {
    $res = "+OK ".strlen($mails[$n])." octets\n" . $mails[$n];
    unset($mails[$n]);
    return $res . "\n.";
  } else {
    return "+ERR not existing";
  }
}

function traiterTop($arg1, $arg2) {
  global $connecte, $mails;
  
  /* echo "traiterTop --" . $arg1 . "--" . $arg1 . "--\n"; */
  
  if(!$connecte) return "+ERR Authorization first";

  $idMsg = intval($arg1);
  $nbLignes = intval($arg2);

  if(isset($mails[$idMsg])) {
    if($nbLignes == 0) {
      $tmp = preg_split("@\n\n@", $mails[$idMsg]);
      return "+OK \n" . $tmp[0] . "\n.";
    } else {
      $res = "+OK \n";
      $tmp = preg_split("@\n@", $mails[$idMsg]);
      for($i=0; $i<$nbLignes; $i++) {
        $res .= $tmp[$i] . "\n";
      }
      return $res . ".";
      
    }
  } else {
    return "+ERR not existing";
  }
  
}

/* int preg_match ( string $pattern , string $subject [, array &$matches [, int $flags = 0 [, int $offset = 0 ]]] )     */
/* int socket_write ( resource $socket , string $buffer [, int $length = 0 ] )       */
  /* string socket_read ( resource $socket , int $length [, int $type = PHP_BINARY_READ ] ) */

function justDo($sock, $msg) {
  echo "reçu : " . $msg . "\n";

  if(preg_match("@(USER|user)\s+(.+)@", $msg, $res)) {
    socket_write ($sock , traiterUser($res[2]) . "\n");

  } else if(preg_match("@(PASS|pass)\s+(.+)@", $msg, $res)) {
    socket_write ($sock , traiterPass($res[2]) . "\n");

  } else if(preg_match("@(LIST|list)\s*@", $msg, $res)) {
    socket_write ($sock , traiterList() . "\n");

  } else if(preg_match("@(TOP|top)\s+(\d+)\s+(\d+)\s*@", $msg, $res)) {
    socket_write ($sock , traiterTop($res[2], $res[3]) . "\n");

  } else if(preg_match("@(QUIT|quit)\s*@", $msg, $res)) {
    socket_write ($sock , traiterQuit() . "\n");
    return false;
  } else if(preg_match("@(RETR|retr)\s+(\d+)\s*@", $msg, $res)) {
    socket_write ($sock , traiterRetr($res[2]) . "\n");

  } else if(preg_match("@(RESET|reset)\s*@", $msg, $res)) {
    socket_write ($sock , resetMails() . "\n");

  } else {
    socket_write ($sock , "+ERR Unknown\n");

  }
}

function traiterClient($sock) {
  do {
    $msg = socket_read($sock, 4096, PHP_NORMAL_READ);
    $tab = preg_split("@\n\n@", $msg);
    foreach($tab as $m) 
      if(trim($m)) {
        if(justDo($sock, rtrim($m, "\n\r")) === false) $msg = false;
      }
  } while($msg !== false);
  
  socket_close($sock);
}



/* MAIN */

global $user;
global $connecte;
global $membres;
global $mails;
resetMails();                   /* Initialiser les mails */
$membres = array("moi" => "a",
                 "*" => "*",
                 "lui" => "mdpLui",
                 "elle" => "mdpElle");

$port = intval($argv[1]);
if(!isset($argv[0])){
  echo "erreur appel serveur : port\n";
  return;
}




  /* resource socket_create ( int $domain , int $type , int $protocol ) */
  $sockServ = socket_create ( AF_INET, SOCK_STREAM, 0);
  if(!$sockServ) {
    return;
  }

  /* bool socket_bind ( resource $socket , string $address [, int $port = 0 ] ) */
  if(!socket_bind ($sockServ, "127.0.0.1", $port)) {
    return;
  }

  /* bool socket_listen ( resource $socket [, int $backlog = 0 ] ) */
  if(!socket_listen ($sockServ, 1)) {
    return;
  }

while(true) {

  echo "en attente sur " . $port . "\n";

  /* resource socket_accept ( resource $socket ) */
  $sock = socket_accept($sockServ);

  echo "client reçu\n";

  traiterClient($sock);
  traiterQuit();

  /* resetMails(); */
  /* break; */
}

echo "extinction\n";
socket_close($sockServ);
?>