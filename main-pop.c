#include "main-pop.h"

int main (int argc, char **argv) {

  if(argc != 4) {
    afficher_usage(argc, argv);
  }

  if (argv[3][1] == 't') {
    dialoguer_textuel(argv[1], atoi(argv[2]));
  } else if (argv[3][1] == 'c') {
    dialoguer_clicable(argv[1], atoi(argv[2]));
  } else if (argv[3][1] == 'g') {
    dialoguer_graphique(argv[1], atoi(argv[2]));
  } else {
    afficher_usage(argc, argv);
  }

  return 0;
}
