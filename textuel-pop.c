#include "main-pop.h"

void dialoguer_textuel(char *serveur, int port) {
  char buf[SIZE_BUFFER];
  int sfd, continuer = 1, retr, multi, nbLus, lenMailRetr;
  FILE *sock;

  /* On ouvre une connexion avec le serveur et on convertit la socket en
     FILE*. Ensuite, jusqu'à preuve du contraire, on lit une ligne au clavier et
     on la transmet au serveur et on affiche le résultat. Si la commande
     implique une réponse multiligne (dans le cas d'un succes), on les reçoit
     toutes, si la réponse est plus complexe on la traite, sinon on retourne au
     début de la boucle */

  sfd = InitConnexion(serveur, port);
  if(sfd == -1) {
    perror("connexion serveur ");
    return;
  }

  sock = fdopen(sfd, "w+");
  if(sock == NULL) {
    printf("Erreur int => FILE*\n");
    return;
  }
  
  printf("Connexion réussie. Socket %d\n", sfd);

  do {
  
    retr = 0;
    multi = 0;

    printf("> ");
    /* Lecture du clavier */
    if(gets(buf) == NULL) {
      perror("gets ");
      exit(1);
    }
    buf[SIZE_BUFFER - 1] = '\0';
    nbLus = strlen(buf);
    buf[nbLus] = '\n';
    buf[nbLus+1] = '\0';
    nbLus++;

    /* On différencie les requetes simples (pas de tag) des requêtes à réponse
       multiligne (tag multi) des requêtes plus particulières (tag personalidé,
       ici retr pour la commande retr) */
    /* Aucune commande n'a de préfixe en commun donc il n'y a pas de priorité
       dans la comparaison ni besoin de rechercher un espace à la fin */
    if(matchi(buf, "RETR")) {
      retr = 1;
      lenMailRetr = atoi(buf + strlen("RETR "));
    } else if (matchi(buf, "LIST")) {
      multi = 1;
    } else if (matchi(buf, "TOP")) {
      multi = 1;
    } else if (matchi(buf, "QUIT")) {
      continuer = 0;
    }

    /* Envoi de la commande au serveur */
    if(fwrite(buf, sizeof(char), nbLus+1, sock) != nbLus+1) {
      perror("Erreur écriture commande\n");
      return;
    }

    if(!fgets(buf, sizeof(char)*SIZE_BUFFER, sock)) {
      perror("fgets ");
      return;
    }
    nbLus = strlen(buf);
    buf[nbLus] = '\0';
    printf("%s", buf);

    if(match(buf, "+OK") && multi) {
      /* Si la commande réussit et est multiligne */
      while(!match(buf, ".\n")) {
        if(!fgets(buf, sizeof(char)*SIZE_BUFFER, sock)) {
          perror("fgets ");
          return;
        }
        nbLus = strlen(buf);
        buf[nbLus] = '\0';
        printf("%s", buf);
      }
    } else if (match(buf, "+OK") && retr) {
      /* Si la commande réussit et est retr */
      retrieve(sock, lenMailRetr, "./", ".");
    }

  } while (continuer);
}



