#include "main-pop.h"

void afficher_usage(int argc, char **argv) {
  printf("usage : %s @IP_serveur port_serveur -[tcg]\n", argv[0]);
  exit(1);
}

int match(char *buf, char *word) {
  return !strncmp(buf, word, strlen(word));
}

int matchi(char *buf, char *word) {
  return !strncasecmp(buf, word, strlen(word));
}


/* Ecrire Le contenu du mail dans un fichier */
/* Le délimitateur délimite la fin du mail */

/* On distinguera la présence du délimitateur qui confirme la fin d'un corps de
   message (ou du mail dans le cas du point) avec la présence du délimitateur
   suivi de deux tirets qui annonce la fin d'un mail multipart. Dans le dernier
   cas, la fonction renvoie 0, dans les autres, elle renvoie 1 */

bool ecrireFichier(FILE *flux, char *nomFichier, char *delim) {
  int arret, nbLus, fileFd;
  bool retour = true;
  char *dernier, tmp;
  char buf[SIZE_BUFFER];
  
  if((fileFd = open(nomFichier, O_WRONLY | O_CREAT | O_TRUNC, 0755)) == -1) {
    perror("open ");
    return false;
  }

  /* dernier délimiteur d'un mail multipart */
  dernier = (char*) malloc(sizeof(char)*(strlen(delim)+2));
  sprintf(dernier, "%s--", delim);

  /* On définit 3 états pour l'arret de la lecture */
  /* à l'état 0 on écrit ce qu'on reçoit et on continue */
  /* on passe à l'état 1 si on lit une ligne vide, dans ce cas, on écrit pas
     tout de suite la ligne */
  /* si à l'état 1 on lit le délimiteur, on passe à l'état 2 et on sors de la
     fonction => le mail est lu. Si la dernière ligne n'est pas le délimiteur,
     alors ce n'est pas la fin du corps et on écrit la ligne précédente et la
     dernière lue */
  arret=0;
  while(arret != 2) {
    if(!fgets(buf, sizeof(char)*SIZE_BUFFER, flux)) {
      perror("fgets ");
      return false;
    }
    nbLus = strlen(buf);
    if(buf[nbLus-1] == '\n') buf[nbLus-1] = '\0';

    if(!strcmp(buf, delim)) {
      arret = 2;
      retour = true;
    } else if(arret == 1 && !strcmp(buf, delim)) {
      /* Si la ligne précédente était vide et que celle ci est le délimiteur */
      /* On arrête */
      arret = 2;
      retour = true;
    } else if(arret == 1 && !strcmp(buf, dernier)) {
      /* Si la ligne précédente était vide et que celle ci est un point mais le
         point n'est pas le délimiteur => c'est la fin d'un multipart */
      /* On arrête */
      arret = 2;
      /* On communique la fin de la fin */
      retour = false;
    } else if (arret == 1) {
      /* Si la ligne précédente était vide mais que celle ci n'est pas le délimiteur */
      /* On écrit la ligne précédente (qui était un retour à la ligne) */
      /* Et on retourne à l'état initial */
      if(write(fileFd, "\n", 1) == -1) {
        perror("write ");
        return false;
      }
      arret = 0;
    } 

    if(arret == 0 && buf[0] == '\0') {
      /* Si étape initiale et ligne vide */
      /* On passe à l'étape 1, on écrit pas la ligne */
      arret = 1;
    } else if (arret != 2) {
      /* Si on est pas à l'état final */
      /* On écrit la chaine */
      tmp = buf[nbLus-1];
      /* Si la char avait été remplacé on le restitue */
      /* S'il n'avait pas été remplacé mais valait bien '\0', il n'aurait pas
         été capturé par le strlen */
      if(buf[nbLus-1] == '\0')
        buf[nbLus-1] = '\n';
      if(write(fileFd, buf, nbLus) == -1) {
        perror("write ");
        return false;
      }
      buf[nbLus-1] = tmp;
    }
  }

  if(close(fileFd) == -1) {
    perror("close ");
    return false;
  }
  return retour;
}

/* Récupère le résultat de la commande RETR */
/* Le "+OK ..." est lu par la fonction appelante */

bool retrieve (FILE *sock, int n, char *prefixePath, char *delim) {
  char *contentType = NULL;
  char *boundary = NULL;
  char buf[SIZE_BUFFER];
  char *p, nomFichier[SIZE_NOM_FICHIER];
  int nbLus, len;
  bool retour = true;
  

  /* On lit les entêtes qui détermine si le corps est multipart ou non */
  /* On extrait le content type et le boundary s'il sont présent */
  do {
    if(!fgets(buf, sizeof(char)*SIZE_BUFFER, sock)) {
      perror("fgets ");
      return false;
    }
    nbLus = strlen(buf);
    if(buf[nbLus-1] == '\n') buf[nbLus-1] = '\0';
    
    if(match(buf, "Content-Type: ")) {

      /* On se place sur la première lettre du type mime, on recherche un
         séparateur et on en déduit la longeur pour la copie */
      /* On ajoute un espace à la fin pour la recherche du type mime */
      p = buf + strlen("Content-Type: ");
      len=0;
      while(*(p+len) != ';' && *(p+len) != ' ' && *(p+len) != '\n' 
            && *(p+len) != '\0') len++;
      len++;                    /* Le premier caractère compte */
      contentType = (char*) malloc(sizeof(char) * (len+1));
      *contentType = '\0';
      strncat(contentType, p, len+1);
      contentType[len-1] = ' ';
      contentType[len] = '\0';

      /* Si boundary est présent, on pointe sur le premier caractère, on
         recherche un délimiteur, on en déduit la taille et on le copie dans une
         chaine précédée de "--" pour la comparaison */
      if((p = strstr(buf, "boundary="))) {
        p = p + strlen("boundary=");
        /* ignorer l'éventuel délimiteur */
        if(*p == '\'' || *p == '"') p++;
        len=0;
        while(*(p+len) != ';' && *(p+len) != ' ' && *(p+len) != '"' && 
              *(p+len) != '\'' && *(p+len) != '\n' && *(p+len) != '\0') 
          len++;
        boundary = (char*) malloc(sizeof(char) * len+2);
        boundary[0] = '-'; boundary[1] = '-'; boundary[2] = '\0';
        strncat(boundary+2, p, len);
      }
    }
  } while(buf[0] != '\0');

  /* Les 2 premiers sont des cas terminaux */
  if(!contentType) {
    /* Pas de content type */
    /* Sauvegarder sous n.txt */
    sprintf(nomFichier, "%s%d.txt", prefixePath, n);
    retour = ecrireFichier(sock, nomFichier, delim);
  } else if(strcmp(contentType, "multipart/mixed ") && strcmp(contentType, "multipart/alternative ")) {
    /* Content type != multipart/mixed */
    /* sauvegarder sous n.nomCanonique */
    /* Ici, prise en compte de multipart/alternative qui réagit comme
       multipart/mixed */

    /* le tableau est de la forme "type1 ext1 type2 ext2 ...". la dernière
       extension est aussi suivie d'un espace */
    /* Donc on cherche le type mime (qui se termine par un espace), on pointe
       sur le premier caractère de l'extension correspondante, on calcule la
       longueur et on détermine le nom du fichier */
    if(!(p = strstr(types_mime, contentType))) {
      printf("Erreur type mime\n%s\n", contentType);
      return false;
    }
    while(*p != ' ') p++; p++;

    len = 0;
    while(*(p+len) != ' ') len++;

    p[len] = '\0';
    sprintf(nomFichier, "%s%d.%s", prefixePath, n, p);
    p[len] = ' ';
    retour = ecrireFichier(sock, nomFichier, delim);
  } else {
    /* Cas récursif : multipart/mixed (ou alternatif) */
    /* On créé le dossier et on s'appelle récursivement avec pour préfixe le
       numméro du message */
    /* On lit aussi la ligne vide et le point à la fin du message qui aurait été
       lu par un cas terminal */
    sprintf(nomFichier, "%d/", n);
    if(mkdir(nomFichier, 0755) == -1) {
      perror("mkdir ");
      /* On continue l'exécution, peut être un dossier résiduel d'un message de
         la session précédente */
      /* return false; */
    }

    /* Tant qu'il reste des parties, on sauvegarde le message */
    while(retrieve(sock, n, nomFichier, boundary));

    if(!fgets(buf, sizeof(char)*SIZE_BUFFER, sock)) {
      perror("fgets ");
      return false;
    }
    nbLus = strlen(buf);
    if(!fgets(buf, sizeof(char)*SIZE_BUFFER, sock)) {
      perror("fgets ");
      return false;
    }
    nbLus = strlen(buf);
  }

  return retour;
}

int InitConnexion(char *serveur, int port)
{
  int maSocket, error;
  struct sockaddr_in const *sin;
  char ascii_buffer[LINELENGTH];
  char sport[255];
  /* char *cause = NULL; */

  struct addrinfo addrHints, *addrResult; // adresses desirees / recuperees 
  struct addrinfo *addrTmp;		//adresse temporaire
	
  sprintf(sport,"%i",port);	//conversion du no de port en chaine de char
  memset(&addrHints, 0, sizeof(addrHints));     // raz 
  addrHints.ai_family = AF_INET;		//famille  TCP/IP ipv4
  addrHints.ai_socktype = SOCK_STREAM;	// avoir une socket de type stream
  // printf("je tente getaddrinfo %s\n", serveur); 
  error = getaddrinfo(serveur, sport, &addrHints, &addrResult);
  if (error) {
    printf("Client Erreur %d getaddrinfo error for host %s %d:\n", error,serveur, port); 
    printf("\t%s\n",gai_strerror(error));
    exit (EXIT_FAILURE);
  }
  // conversion du format Network des adresses les IP du serveur en binaire
  for (addrTmp = addrResult; addrTmp; addrTmp = addrTmp->ai_next) {
    //ai_proto=6 pour TCp alors  AFINET pour ipv4 par default
    sin = (void *)addrTmp->ai_addr;
    if (inet_ntop(AF_INET, &sin->sin_addr, ascii_buffer, sizeof(ascii_buffer)) == NULL)
      printf(" inet_ntop : Echec \n");
  }
  //connection a la premiere socket
  maSocket = -1;
  for (addrTmp = addrResult; addrTmp; addrTmp = addrTmp->ai_next) {
    maSocket = socket(addrTmp->ai_family, addrTmp->ai_socktype,addrTmp->ai_protocol);
    if (maSocket < 0) {
      /* cause = "socket"; */
      continue;
    }
    // printf("je tente connect %d\n", addrTmp->ai_addrlen);
    if (connect(maSocket, addrTmp->ai_addr, addrTmp->ai_addrlen) < 0) {
      /* cause = "connect"; */
      close(maSocket);
      maSocket = -1;
      continue;}
    break;  // okay we got one 
  }
 
  if (maSocket < 0) {// NOTREACHED
    perror("erreur de connection ou de socket");}
  // printf("socket  %d\n", socket);
  return maSocket;
}

char types_mime[] = "application/andrew-inset ez application/annodex anx application/atom+xml atom application/atomcat+xml atomcat application/atomserv+xml atomsrv application/bbolin lin application/cu-seeme cu application/davmount+xml davmount application/dicom dcm application/dsptype tsp application/ecmascript es application/futuresplash spl application/hta hta application/java-archive jar application/java-serialized-object ser application/java-vm class application/javascript js application/json json application/m3g m3g application/mac-binhex40 hqx application/mac-compactpro cpt application/mathematica nb application/mbox mbox application/msaccess mdb application/msword doc application/mxf mxf application/octet-stream bin application/oda oda application/ogg ogx application/onenote one application/pdf pdf application/pgp-encrypted pgp application/pgp-keys key application/pgp-signature sig application/pics-rules prf application/postscript ps application/rar rar application/rdf+xml rdf application/rtf rtf application/sla stl application/smil smi application/xhtml+xml xhtml application/xml xml application/xspf+xml xspf application/zip zip application/vnd.android.package-archive apk application/vnd.cinderella cdy application/vnd.google-earth.kml+xml kml application/vnd.google-earth.kmz kmz application/vnd.mozilla.xul+xml xul application/vnd.ms-excel xls application/vnd.ms-excel.addin.macroEnabled.12 xlam application/vnd.ms-excel.sheet.binary.macroEnabled.12 xlsb application/vnd.ms-excel.sheet.macroEnabled.12 xlsm application/vnd.ms-excel.template.macroEnabled.12 xltm application/vnd.ms-fontobject eot application/vnd.ms-officetheme thmx application/vnd.ms-pki.seccat cat application/vnd.ms-powerpoint ppt application/vnd.ms-powerpoint.addin.macroEnabled.12 ppam application/vnd.ms-powerpoint.presentation.macroEnabled.12 pptm application/vnd.ms-powerpoint.slide.macroEnabled.12 sldm application/vnd.ms-powerpoint.slideshow.macroEnabled.12 ppsm application/vnd.ms-powerpoint.template.macroEnabled.12 potm application/vnd.ms-word.document.macroEnabled.12 docm application/vnd.ms-word.template.macroEnabled.12 dotm application/vnd.oasis.opendocument.chart odc application/vnd.oasis.opendocument.database odb application/vnd.oasis.opendocument.formula odf application/vnd.oasis.opendocument.graphics odg application/vnd.oasis.opendocument.graphics-template otg application/vnd.oasis.opendocument.image odi application/vnd.oasis.opendocument.presentation odp application/vnd.oasis.opendocument.presentation-template otp application/vnd.oasis.opendocument.spreadsheet ods application/vnd.oasis.opendocument.spreadsheet-template ots application/vnd.oasis.opendocument.text odt application/vnd.oasis.opendocument.text-master odm application/vnd.oasis.opendocument.text-template ott application/vnd.oasis.opendocument.text-web oth application/vnd.openxmlformats-officedocument.presentationml.presentation pptx application/vnd.openxmlformats-officedocument.presentationml.slide sldx application/vnd.openxmlformats-officedocument.presentationml.slideshow ppsx application/vnd.openxmlformats-officedocument.presentationml.template potx application/vnd.openxmlformats-officedocument.spreadsheetml.sheet xlsx application/vnd.openxmlformats-officedocument.spreadsheetml.sheet xlsx application/vnd.openxmlformats-officedocument.spreadsheetml.template xltx application/vnd.openxmlformats-officedocument.spreadsheetml.template xltx application/vnd.openxmlformats-officedocument.wordprocessingml.document docx application/vnd.openxmlformats-officedocument.wordprocessingml.template dotx application/vnd.rim.cod cod application/vnd.smaf mmf application/vnd.stardivision.calc sdc application/vnd.stardivision.chart sds application/vnd.stardivision.draw sda application/vnd.stardivision.impress sdd application/vnd.stardivision.math sdf application/vnd.stardivision.writer sdw application/vnd.stardivision.writer-global sgl application/vnd.sun.xml.calc sxc application/vnd.sun.xml.calc.template stc application/vnd.sun.xml.draw sxd application/vnd.sun.xml.draw.template std application/vnd.sun.xml.impress sxi application/vnd.sun.xml.impress.template sti application/vnd.sun.xml.math sxm application/vnd.sun.xml.writer sxw application/vnd.sun.xml.writer.global sxg application/vnd.sun.xml.writer.template stw application/vnd.symbian.install sis application/vnd.tcpdump.pcap pcap application/vnd.visio vsd application/vnd.wap.wbxml wbxml application/vnd.wap.wmlc wmlc application/vnd.wap.wmlscriptc wmlsc application/vnd.wordperfect wpd application/vnd.wordperfect5.1 wp5 application/x-123 wk application/x-7z-compressed 7z application/x-abiword abw application/x-apple-diskimage dmg application/x-bcpio bcpio application/x-bittorrent torrent application/x-cab cab application/x-cbr cbr application/x-cbz cbz application/x-cdf cdf application/x-cdlink vcd application/x-chess-pgn pgn application/x-comsol mph application/x-cpio cpio application/x-csh csh application/x-debian-package deb application/x-director dcr application/x-dms dms application/x-doom wad application/x-dvi dvi application/x-font pfa application/x-font-woff woff application/x-freemind mm application/x-futuresplash spl application/x-ganttproject gan application/x-gnumeric gnumeric application/x-go-sgf sgf application/x-graphing-calculator gcf application/x-gtar gtar application/x-gtar-compressed tgz application/x-hdf hdf application/x-hwp hwp application/x-ica ica application/x-info info application/x-internet-signup ins application/x-iphone iii application/x-iso9660-image iso application/x-jam jam application/x-java-jnlp-file jnlp application/x-jmol jmz application/x-kchart chrt application/x-killustrator kil application/x-koan skp application/x-kpresenter kpr application/x-kspread ksp application/x-kword kwd application/x-latex latex application/x-lha lha application/x-lyx lyx application/x-lzh lzh application/x-lzx lzx application/x-maker frm application/x-md5 md5 application/x-mif mif application/x-mpegURL m3u8 application/x-ms-wmd wmd application/x-ms-wmz wmz application/x-msdos-program exe application/x-msi msi application/x-netcdf nc application/x-ns-proxy-autoconfig pac application/x-nwc nwc application/x-object o application/x-oz-application oza application/x-pkcs7-certreqresp p7r application/x-pkcs7-crl crl application/x-python-code pyc application/x-qgis qgs application/x-quicktimeplayer qtl application/x-rdp rdp application/x-redhat-package-manager rpm application/x-rss+xml rss application/x-ruby rb application/x-scilab sci application/x-scilab-xcos xcos application/x-sh sh application/x-sha1 sha1 application/x-shar shar application/x-shockwave-flash swf application/x-silverlight scr application/x-sql sql application/x-stuffit sit application/x-sv4cpio sv4cpio application/x-sv4crc sv4crc application/x-tar tar application/x-tcl tcl application/x-tex-gf gf application/x-tex-pk pk application/x-texinfo texinfo application/x-trash ~ application/x-troff t application/x-troff-man man application/x-troff-me me application/x-troff-ms ms application/x-ustar ustar application/x-wais-source src application/x-wingz wz application/x-x509-ca-cert crt application/x-xcf xcf application/x-xfig fig application/x-xpinstall xpi audio/amr amr audio/amr-wb awb audio/amr amr audio/amr-wb awb audio/annodex axa audio/basic au audio/csound csd audio/flac flac audio/midi midi audio/mpeg mpga audio/mpegurl m3u audio/ogg ogg audio/prs.sid sid audio/x-aiff aif audio/x-gsm gsm audio/x-mpegurl m3u audio/x-ms-wma wma audio/x-ms-wax wax audio/x-pn-realaudio ra audio/x-realaudio ra audio/x-scpls pls audio/x-sd2 sd2 audio/x-wav wav chemical/x-alchemy alc chemical/x-cache cac chemical/x-cache-csf csf chemical/x-cactvs-binary cbin chemical/x-cdx cdx chemical/x-cerius cer chemical/x-chem3d c3d chemical/x-chemdraw chm chemical/x-cif cif chemical/x-cmdf cmdf chemical/x-cml cml chemical/x-compass cpa chemical/x-crossfire bsd chemical/x-csml csml chemical/x-ctx ctx chemical/x-cxf cxf chemical/x-embl-dl-nucleotide emb chemical/x-galactic-spc spc chemical/x-gamess-input inp chemical/x-gaussian-checkpoint fch chemical/x-gaussian-cube cub chemical/x-gaussian-input gau chemical/x-gaussian-log gal chemical/x-gcg8-sequence gcg chemical/x-genbank gen chemical/x-hin hin chemical/x-isostar istr chemical/x-jcamp-dx jdx chemical/x-kinemage kin chemical/x-macmolecule mcm chemical/x-macromodel-input mmd chemical/x-mdl-molfile mol chemical/x-mdl-rdfile rd chemical/x-mdl-rxnfile rxn chemical/x-mdl-sdfile sd chemical/x-mdl-tgf tgf chemical/x-mmcif mcif chemical/x-mol2 mol2 chemical/x-molconn-Z b chemical/x-mopac-graph gpt chemical/x-mopac-input mop chemical/x-mopac-out moo chemical/x-mopac-vib mvb chemical/x-ncbi-asn1 asn chemical/x-ncbi-asn1-ascii prt chemical/x-ncbi-asn1-binary val chemical/x-ncbi-asn1-spec asn chemical/x-pdb pdb chemical/x-rosdal ros chemical/x-swissprot sw chemical/x-vamas-iso14976 vms chemical/x-vmd vmd chemical/x-xtel xtel chemical/x-xyz xyz image/gif gif image/ief ief image/jpeg jpeg image/pcx pcx image/png png image/svg+xml svg image/tiff tiff image/vnd.djvu djvu image/vnd.microsoft.icon ico image/vnd.wap.wbmp wbmp image/x-canon-cr2 cr2 image/x-canon-crw crw image/x-cmu-raster ras image/x-coreldraw cdr image/x-coreldrawpattern pat image/x-coreldrawtemplate cdt image/x-corelphotopaint cpt image/x-epson-erf erf image/x-jg art image/x-jng jng image/x-ms-bmp bmp image/x-nikon-nef nef image/x-olympus-orf orf image/x-photoshop psd image/x-portable-anymap pnm image/x-portable-bitmap pbm image/x-portable-graymap pgm image/x-portable-pixmap ppm image/x-rgb rgb image/x-xbitmap xbm image/x-xpixmap xpm image/x-xwindowdump xwd message/rfc822 eml model/iges igs model/mesh msh model/vrml wrl model/x3d+vrml x3dv model/x3d+xml x3d model/x3d+binary x3db text/cache-manifest appcache text/calendar ics text/css css text/csv csv text/h323 323 text/html html text/iuls uls text/mathml mml text/plain txt text/richtext rtx text/scriptlet sct text/texmacs tm text/tab-separated-values tsv text/vnd.sun.j2me.app-descriptor jad text/vnd.wap.wml wml text/vnd.wap.wmlscript wmls text/x-bibtex bib text/x-boo boo text/x-c++hdr hpp text/x-c++src cpp text/x-chdr h text/x-component htc text/x-csh csh text/x-csrc c text/x-dsrc d text/x-diff diff text/x-haskell hs text/x-java java text/x-lilypond ly text/x-literate-haskell lhs text/x-moc moc text/x-pascal p text/x-pcs-gcd gcd text/x-perl pl text/x-python py text/x-scala scala text/x-setext etx text/x-sfv sfv text/x-sh sh text/x-tcl tcl text/x-tex tex text/x-vcalendar vcs text/x-vcard vcf video/3gpp 3gp video/annodex axv video/dl dl video/dv dif video/fli fli video/gl gl video/mpeg mpeg video/MP2T ts video/mp4 mp4 video/quicktime qt video/ogg ogv video/webm webm video/vnd.mpegurl mxu video/x-flv flv video/x-la-asf lsf video/x-mng mng video/x-ms-asf asf video/x-ms-wm wm video/x-ms-wmv wmv video/x-ms-wmx wmx video/x-ms-wvx wvx video/x-msvideo avi video/x-sgi-movie movie video/x-matroska mkv x-conference/x-cooltalk ice x-epoc/x-sisx-app sisx x-world/x-vrml vrm ";

