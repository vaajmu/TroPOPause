#ifndef __MAIN_POP_H__
#define __MAIN_POP_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <X11/cursorfont.h>
#include <X11/Xlib.h>
#include <X11/keysym.h> 
#include <X11/Xutil.h> 
/* #include <X11/Xatom.h> */
/* #include <X11/Xutil.h> */
/* #include <regex.h> */
#include <sys/socket.h>         /* pour socket */
#include <netinet/in.h>         /* pour sockaddr_in et INET_ADDRSTRLEN */
#include <netdb.h>              /* pour gethostbyname */
#include <unistd.h>             /* pour read /write */
#include <arpa/inet.h>          /* pour inet_aton */
#include <errno.h> /* pour perror et errno */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define LINELENGTH 1024
#define SIZE_BUFFER (1<<12)
#define SIZE_NOM_FICHIER (1<<10)
#define SIZE_BUFFER_LETTRE 16

extern void dialoguer_textuel(char *ip, int port);
extern void dialoguer_clicable(char *ip, int port);
extern void dialoguer_graphique(char *ip, int port);

/* Affiche les arguments attendus par le programme */
extern void afficher_usage(int argc, char **argv);
/* Compare les len(word) premiers caractères de buf et word */
extern int match(char *buf, char *word);
/* Compare les len(word) premiers caractères de buf et word (insensible à la
   casse) */
extern int matchi(char *buf, char *word);
extern int InitConnexion(char *serveur, int port);
/* Ouvre le fichier nomFichier en écriture et reçoit/ecrit depuis le flux vers
   le fichier jusqu'à rencontrer le délimiteur. Retourne faux si le message est
   le dernier d'un message multipart */
extern bool ecrireFichier(FILE *flux, char *nomFichier, char *delim);
/* Traite le résultat d'une requête GET (sauf le code de retour qui doit être
   traité avant). Est notemment en charge de consommer les entêtes. Retourne
   faux si le message consommé est le dernier */
extern bool retrieve (FILE *sock, int n, char *prefixePath, char *delim);

extern char types_mime[];

#endif
