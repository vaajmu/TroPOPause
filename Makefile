CC = gcc
FLAGS = -Wall -g -I/usr/X11R6/include -L/usr/X11R6/lib -lX11 # $(XP_INC) $(XP_DEFINES)
DST = /tmp/3000817/

TARGET = $(DST)clientPOP
SRC = utils.c textuel-pop.c clicable-pop.c graphique-pop.c main-pop.c
OBJ = $(SRC:%.c=$(DST)%.o)
HEADER = main-pop.h

FICHIERS_PRODUITS = $(OBJ) $(TARGET) $(DST)

IP_SERVEUR = 127.0.0.1
PORT_SERVEUR = 12545

EXTRACT_MIME_SRC = mime.types
EXTRACT_MIME_DST = utils.c

all: $(TARGET)

$(TARGET): $(OBJ)
	$(CC) -o $@ $^ $(FLAGS)

$(DST)%.o: %.c $(DST) $(HEADER)
	$(CC) -c -o $@ $< $(FLAGS)

clean:
	rm -rf $(FICHIERS_PRODUITS)

proper:
	rm -f $(OBJ)

$(DST):
	mkdir $(DST)




extractMime:
	./extractTypesMimes $(EXTRACT_MIME_SRC) $(EXTRACT_MIME_DST)

t-test: $(TARGET)
	cd $(DST) && $(TARGET) $(IP_SERVEUR) $(PORT_SERVEUR) -t

c-test: $(TARGET)
	cd $(DST) && $(TARGET) $(IP_SERVEUR) $(PORT_SERVEUR) -c

g-test: $(TARGET)
	cd $(DST) && $(TARGET) $(IP_SERVEUR) $(PORT_SERVEUR) -g

# Le sleep est là pour laisser au serveur le temps de se démarrer avant de lancer le client
run-serveur: $(DST)
	php serveur.php $(PORT_SERVEUR) > $(DST)serveurPopPhpOUT.log 2> $(DST)serveurPopPhpERR.log &
	sleep 1

# le echo est là pour que la cible ne retoure pas un code d'erreur si le kill échoue
stop-serveur:
	killall php 2> /dev/null || echo -n ""

reset-mails:
	echo "RESET\nQUIT" | telnet $(IP_SERVEUR) $(PORT_SERVEUR) 1>/dev/null 2>/dev/null || echo -n ""

