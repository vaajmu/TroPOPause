#include "main-pop.h"

#define WIDTH_SAISIE_LOGIN 500
#define HEIGHT_SAISIE_LOGIN 50
#define MARGIN 20
#define LEN_CHAMPS_LOGIN 1<<9

#define WIDTH_AFFICHAGE_MSG 700
#define HEIGHT_LIGNE_AFFICHAGE_MESSAGE 20
#define HEIGHT_SEPARATEUR_AFFICHAGE_MESSAGE 10
#define MARGIN_AFFICHAGE_DANS_LIGNE 6
#define PADDING_MSG_ERREUR 15

#define HEIGHT_MSG_QUIT 20
#define WIDTH_MSG_QUIT 33
#define MARGIN_HAUTE_MSG_QUIT 10
#define MARGIN_DANS_MSG_QUIT 5

Display *dpy;
GC gc, gcinv;
Window racine, 
  fenetreLogin,                 /* Pour l'identification */
  fenetreMsg,                   /* Pour la liste des messages */
  fenetreQuit,                  /* Pour le outon QUIT */
  fenetreLoginMsgErreur;        /* Pour l'erreur d'identification */

/* Représente un champ de saisie */
typedef struct {
  Window fenetre;
  char texte[LEN_CHAMPS_LOGIN];
  int lenTexte;
} champLogin;

champLogin *champActif, champUser, champPass;
char msgErreurLogin[LINELENGTH];

/* Représente un message reçu */
typedef struct _msgPop {
  int id;                       /* identifiant du message */
  char *nomEmmeteur;
  char *dateEmission;
  Window fenetreId;             /* Ligne clicable où afficher l'id du message */
  struct _msgPop *next;         /* Est en fait une liste chainée */
} msgPop;

/* Tête de liste */
typedef struct {
  int nbMsg;
  msgPop *premier;
  msgPop *dernier;
} listeMsgPop;
listeMsgPop listeMessages;

/* Relache un clic dans la fenêtre de login */
void pourButtonRelease(XButtonEvent *evmt) {
  if(evmt->window == champUser.fenetre) {
    champActif = &champUser;
  } else if(evmt->window == champPass.fenetre) {
    champActif = &champPass;
  } else {
    champActif = NULL;
  }
}

/* Réaffiche la fenetre de login */
void exposeLogin(void) {
  XDrawString(dpy, champUser.fenetre, gc, MARGIN,
              HEIGHT_SAISIE_LOGIN - MARGIN, champUser.texte,
              champUser.lenTexte);
  XDrawString(dpy, champPass.fenetre, gc, MARGIN,
              HEIGHT_SAISIE_LOGIN - MARGIN, champPass.texte,
              champPass.lenTexte);

  if(msgErreurLogin[0] != '\0')   
    XDrawString(dpy, fenetreLoginMsgErreur, gc, MARGIN_AFFICHAGE_DANS_LIGNE,
                PADDING_MSG_ERREUR, msgErreurLogin,
                strlen(msgErreurLogin));

  XFlush(dpy);
}

/* Ecris en blanc la chaine déjà affichée pour la masquer */
void effacerChamp(champLogin *champ) {
  XDrawString(dpy, champ->fenetre, gcinv, MARGIN, 
              HEIGHT_SAISIE_LOGIN - MARGIN, champ->texte, 
              champ->lenTexte);
}

/* Traite l'appui sur une touche pour la fenetre de login */
/* Retourne faux si la touche est 'Entrée' */
bool pourKeyPress(XKeyEvent *evmt) {

  char buf[SIZE_BUFFER_LETTRE];
  KeySym key;
  int nbKeyPressed;
  
  if(champActif == NULL) return true;

  nbKeyPressed = XLookupString(evmt, buf, SIZE_BUFFER_LETTRE, &key, 0);
  
  if(key == XK_Return) {
    return false;
  } 

  if (key == XK_BackSpace) {
    effacerChamp(champActif);
    champActif->texte[champActif->lenTexte-1] = '\0';
    champActif->lenTexte--;
    XDrawString(dpy, champActif->fenetre, gc, MARGIN, 
                HEIGHT_SAISIE_LOGIN - MARGIN, champActif->texte, 
                champActif->lenTexte);
  } else if (nbKeyPressed != 0) {
    if(champActif->lenTexte >= LEN_CHAMPS_LOGIN) return true;
    champActif->texte[champActif->lenTexte] = buf[nbKeyPressed-1];
    champActif->texte[champActif->lenTexte+1] = '\0';
    champActif->lenTexte++;
    exposeLogin();

    /* Résoud le bug de l'affichage du premier caractère saisi qui ne s'affiche pas */
    /* Bug inexpliqué */
    if(champActif->lenTexte == 1) exposeLogin();
  }


  return true;
}

void creerFenetreLogin(void) {

  fenetreLogin = XCreateSimpleWindow(dpy, racine, 0, 0, WIDTH_SAISIE_LOGIN + MARGIN*2,
                                HEIGHT_SAISIE_LOGIN*2 + HEIGHT_LIGNE_AFFICHAGE_MESSAGE + MARGIN*4, 0, 0,
                                WhitePixel(dpy, DefaultScreen(dpy)));

  XSelectInput(dpy, fenetreLogin, KeyPressMask | ButtonPressMask | ButtonReleaseMask | ExposureMask);

  champUser.fenetre = XCreateSimpleWindow(dpy, fenetreLogin, MARGIN, MARGIN,
                                           WIDTH_SAISIE_LOGIN,
                                           HEIGHT_SAISIE_LOGIN, 1,
                                           BlackPixel(dpy, DefaultScreen(dpy)),
                                           WhitePixel(dpy, DefaultScreen(dpy)));

  XSelectInput(dpy, champUser.fenetre, ButtonPressMask | ButtonReleaseMask);

  champPass.fenetre = XCreateSimpleWindow(dpy, fenetreLogin, MARGIN,
                                  2*MARGIN + HEIGHT_SAISIE_LOGIN,
                                  WIDTH_SAISIE_LOGIN,
                                  HEIGHT_SAISIE_LOGIN, 1,
                                  BlackPixel(dpy, DefaultScreen(dpy)),
                                  WhitePixel(dpy, DefaultScreen(dpy)));

  XSelectInput(dpy, champPass.fenetre, ButtonPressMask | ButtonReleaseMask);

  fenetreLoginMsgErreur = XCreateSimpleWindow(dpy, fenetreLogin, MARGIN, 
                                              3*MARGIN + 2*HEIGHT_SAISIE_LOGIN,
                                              WIDTH_SAISIE_LOGIN,
                                              HEIGHT_LIGNE_AFFICHAGE_MESSAGE, 0,
                                              BlackPixel(dpy, DefaultScreen(dpy)),
                                              WhitePixel(dpy, DefaultScreen(dpy)));

  XStoreName(dpy, fenetreLogin, "Connexion");
  XMapWindow(dpy, fenetreLogin);
  XMapSubwindows(dpy, fenetreLogin);

}

void fermerFenetreLogin(void) {
  XUnmapWindow(dpy, fenetreLogin);
  XUnmapSubwindows(dpy, fenetreLogin);
  exposeLogin();
}

/* Envoie une requête RETR et traite la réponse */
void retrMsg(msgPop *msg, FILE *sock) {
  char buf[SIZE_BUFFER];

  sprintf(buf, "RETR %d\n", msg->id);
  if(fwrite(buf, sizeof(char), strlen(buf), sock) == -1) {
    perror("Erreur écriture RETR\n");
    return;
  }

  /* Recevoir la première ligne et vérifier le résultat */
  if(!fgets(buf, sizeof(char)*SIZE_BUFFER, sock)) {
    perror("fgets ");
    return;
  }
  if(!match(buf, "+OK")) {
    printf("%s", buf);
    return;
  }

  retrieve(sock, msg->id, "./", ".");
}

/* Clic dans la fenetre qui liste les messages. Retourne faux si c'est un clic
   sur le bouton QUIT */
bool pourButtonReleaseListeMsg(XButtonEvent *evmt, FILE *sock) {
  msgPop *msg;
  int i;
  
  if(evmt->window == fenetreQuit) {
    return false;
  }

  /* Sinon, recherche du message qui a été cliqué */
  for(msg = listeMessages.premier, i=0; msg && i<10; msg = msg->next, i++) {
    if(evmt->window == msg->fenetreId) {
      printf("Retrieve %d\n", msg->id);
      retrMsg(msg, sock);
      XSelectInput(dpy, msg->fenetreId, 0);
    }
  }
  return true;
}

/* (Ré)affiche la liste des messages */
void exposeListeMsg() {
  msgPop *msg;
  int i, x, y;
  char buf[SIZE_BUFFER];

  x=MARGIN+MARGIN_AFFICHAGE_DANS_LIGNE; y=MARGIN;
  for(msg = listeMessages.premier, i=0; msg && i<10; msg = msg->next, i++) {

    XDrawRectangle(dpy, fenetreMsg, gc, MARGIN, y, WIDTH_AFFICHAGE_MSG, 
                   3*HEIGHT_LIGNE_AFFICHAGE_MESSAGE);

    sprintf(buf, "%d", msg->id);
    XDrawString(dpy, msg->fenetreId, gc, MARGIN_AFFICHAGE_DANS_LIGNE, 
                HEIGHT_LIGNE_AFFICHAGE_MESSAGE - 
                MARGIN_AFFICHAGE_DANS_LIGNE, buf, strlen(buf));

    y+=HEIGHT_LIGNE_AFFICHAGE_MESSAGE;
    sprintf(buf, "%s", msg->nomEmmeteur);
    XDrawString(dpy, fenetreMsg, gc, x, y + HEIGHT_LIGNE_AFFICHAGE_MESSAGE - 
                   MARGIN_AFFICHAGE_DANS_LIGNE, buf, strlen(buf));
    
    y+=HEIGHT_LIGNE_AFFICHAGE_MESSAGE;
    sprintf(buf, "%s", msg->dateEmission);
    XDrawString(dpy, fenetreMsg, gc, x, y + HEIGHT_LIGNE_AFFICHAGE_MESSAGE - 
                MARGIN_AFFICHAGE_DANS_LIGNE, buf, strlen(buf));

    y+=HEIGHT_LIGNE_AFFICHAGE_MESSAGE;
    y+=HEIGHT_SEPARATEUR_AFFICHAGE_MESSAGE;
  }

  XDrawString(dpy, fenetreQuit, gc, MARGIN_DANS_MSG_QUIT, HEIGHT_MSG_QUIT - 
              MARGIN_DANS_MSG_QUIT, "QUIT", strlen("QUIT"));

  XDrawRectangle(dpy, fenetreQuit, gc, 1, 1, WIDTH_MSG_QUIT-2, HEIGHT_MSG_QUIT-2);
  XFlush(dpy);
}

/* Ouvre la fenetre pour la liste des messages */
void creerFenetreListeMsg(void) {
  int widthAff, heightAff, maxMsg;

  if(listeMessages.nbMsg > 10) maxMsg = 10;
  else maxMsg = listeMessages.nbMsg;

  widthAff = WIDTH_AFFICHAGE_MSG + MARGIN*2;
  heightAff = ((HEIGHT_LIGNE_AFFICHAGE_MESSAGE*3+HEIGHT_SEPARATEUR_AFFICHAGE_MESSAGE)*maxMsg) +
    (MARGIN*2) - HEIGHT_SEPARATEUR_AFFICHAGE_MESSAGE + HEIGHT_MSG_QUIT + MARGIN_HAUTE_MSG_QUIT;

  fenetreMsg = XCreateSimpleWindow(dpy, racine, 0, 0,
                                   widthAff,
                                   heightAff, 0, 0,
                                   WhitePixel(dpy, DefaultScreen(dpy)));

  XSelectInput(dpy, fenetreMsg, ButtonPressMask | ButtonReleaseMask | ExposureMask);

  fenetreQuit = XCreateSimpleWindow(dpy, fenetreMsg, widthAff-MARGIN-WIDTH_MSG_QUIT, 
                                    heightAff-HEIGHT_MSG_QUIT-MARGIN,
                                    WIDTH_MSG_QUIT,
                                    HEIGHT_MSG_QUIT, 0, 0,
                                    WhitePixel(dpy, DefaultScreen(dpy)));

  XSelectInput(dpy, fenetreQuit, ButtonPressMask | ButtonReleaseMask);

  XStoreName(dpy, fenetreMsg, "Liste des messages");
  XMapWindow(dpy, fenetreMsg);
  XMapSubwindows(dpy, fenetreMsg);
}

/* Envoie une requête LIST et traite la réponse. Pour chaque message, alloue une
   structure msgPop, l'insère dans la liste des messages et met à jour le nombre
   de messages */
void listerMessages(FILE *sock) {
  char buf[SIZE_BUFFER];
  msgPop *msg;

  if(fwrite("LIST\n", sizeof(char), strlen("LIST\n"), sock) == -1) {
    perror("Erreur écriture LIST\n");
    return;
  }

  /* Recevoir la première ligne et vérifier le résultat */
  if(!fgets(buf, sizeof(char)*SIZE_BUFFER, sock)) {
    perror("fgets ");
    return;
  }
  if(!match(buf, "+OK")) {
    printf("%s", buf);
  }

  while(!match(buf, ".\n")) {
    if(!fgets(buf, sizeof(char)*SIZE_BUFFER, sock)) {
      perror("fgets ");
      return;
    }
    if(!match(buf, ".\n")) {
      listeMessages.nbMsg++;
      msg = (msgPop*) malloc(sizeof(msgPop));
      msg->id = atoi(buf);
      msg->next = NULL;
      if(!listeMessages.premier) listeMessages.premier = msg;
      if(listeMessages.dernier) listeMessages.dernier->next = msg;
      listeMessages.dernier = msg;
    }
  }

}

/* Envoie une requête POP par message et traite le résultat. Met à jour la
   structure du message en ajoutant l'emmetteur et la date */
void topMessages(FILE *sock) {
  char buf[SIZE_BUFFER], *p;
  msgPop *msg;
  int i, cpt;

  for(msg = listeMessages.premier, i=0; msg && i<10; msg = msg->next, i++) {

    sprintf(buf, "TOP %d 0\n", i);
    if(fwrite(buf, sizeof(char), strlen(buf), sock) == -1) {
      perror("Erreur écriture TOP\n");
      return;
    }

    /* Recevoir la première ligne et vérifier le résultat */
    if(!fgets(buf, sizeof(char)*SIZE_BUFFER, sock)) {
      perror("fgets ");
      return;
    }
    if(!match(buf, "+OK")) {
      printf("%s", buf);
    }

    while(!match(buf, ".\n")) {
      if(!fgets(buf, sizeof(char)*SIZE_BUFFER, sock)) {
        perror("fgets ");
        return;
      }
      if(!match(buf, ".\n")) {

        if(match(buf, "From: ")) {
          for(p=buf+strlen("From: "), cpt=0; *p!='\n'; p++, cpt++);
          msg->nomEmmeteur = strndup(buf + strlen("From: "), cpt+1);
          msg->nomEmmeteur[cpt] = '\0';
        } else if (match(buf, "Date: ")) {
          for(p=buf+strlen("Date: "), cpt=0; *p!='\n'; p++, cpt++);
          msg->dateEmission = strndup(buf + strlen("Date: "), cpt+1);
          msg->dateEmission[cpt] = '\0';
        }
      }
    }
  }
}

/* Créé pour chaque message une fenetre clicable de la taille d'une ligne (pour
   l'identifiant du message) */
void creerFenetresIdMsg() {
  int y, i;
  msgPop *msg;

  y = MARGIN;
  for(msg = listeMessages.premier, i=0; msg && i<10; msg = msg->next, i++) {
    /* -1 et -2 dans les coordonnées pour ne pas se superposer à la bordure */
    msg->fenetreId = XCreateSimpleWindow(dpy, fenetreMsg, MARGIN+1, y+1, WIDTH_AFFICHAGE_MSG-2,
                                         HEIGHT_LIGNE_AFFICHAGE_MESSAGE-2, 0, 0,
                                         WhitePixel(dpy, DefaultScreen(dpy)));
    y += (3*HEIGHT_LIGNE_AFFICHAGE_MESSAGE + HEIGHT_SEPARATEUR_AFFICHAGE_MESSAGE);

    XSelectInput(dpy, msg->fenetreId, ButtonPressMask | ButtonReleaseMask);
  }

  XMapSubwindows(dpy, fenetreMsg);
}

void dialoguer_clicable(char *serveur, int port) {
  XEvent e;
  XGCValues xgcvalues;
  bool demandeConnexion = false, connexionReussie = false, veutQuitter = false;
  int sfd;
  FILE *sock;
  char buf[SIZE_BUFFER];

  /* Initialiser les variables/structures */
  champActif = NULL;
  champUser.lenTexte = 0;
  *champUser.texte = '\0';
  champPass.lenTexte = 0;
  *champPass.texte = '\0';

  listeMessages.nbMsg = 0;
  listeMessages.premier = NULL;
  listeMessages.dernier = NULL;

  msgErreurLogin[0] = '\0';

  if ((dpy = XOpenDisplay(NULL)) == NULL) {
    fprintf(stderr, "Erreur XOpenDisplay\n");
    exit(1);
  }
  racine = DefaultRootWindow(dpy);

  creerFenetreLogin();
  gc = DefaultGC(dpy, DefaultScreen(dpy));

  xgcvalues.background = BlackPixel(dpy, DefaultScreen(dpy));
  xgcvalues.foreground = WhitePixel(dpy, DefaultScreen(dpy));
  gcinv = XCreateGC(dpy, racine, GCForeground | GCBackground, &xgcvalues);

  sfd = InitConnexion(serveur, port);
  if(sfd == -1) {
    perror("connexion serveur ");
    return;
  }
  sock = fdopen(sfd, "w+");
  if(sock == NULL) {
    printf("Erreur int => FILE*\n");
    return;
  }

  /* --- */

  /* Tant que l'autentification n'a pas réussi */
  do {

    /* Tant que l'autentification n'est pas demandée */
    while(!demandeConnexion) {

      XNextEvent(dpy, &e);
      switch(e.type) {
      case ButtonRelease:
        pourButtonRelease((XButtonEvent*)(&e));
        break;
      case KeyPress:
        if(!pourKeyPress((XKeyEvent *)(&e)))
          demandeConnexion = true;
        break;
      case Expose:
        exposeLogin();
        break;
      default:
        break;
      }

    }

    /* Envoi des messages de connexion */
    /* USER */
    sprintf(buf, "USER %s\n", champUser.texte);
    if(fwrite(buf, sizeof(char), champUser.lenTexte+6, sock) == -1) {
      perror("Erreur écriture USER\n");
      return;
    }
    if(!fgets(buf, sizeof(char)*SIZE_BUFFER, sock)) {
      perror("fgets ");
      return;
    }
    if(!match(buf, "+OK")) {
      printf("%s", buf);
    }

    /* PASS */
    sprintf(buf, "PASS %s\n", champPass.texte);
    if(fwrite(buf, sizeof(char), champPass.lenTexte+6, sock) == -1) {
      perror("Erreur écriture PASS\n");
      return;
    }
    if(!fgets(buf, sizeof(char)*SIZE_BUFFER, sock)) {
      perror("fgets ");
      return;
    }

    /* Echec de l'autentification : réinitialiser les champs et afficher un
       message dans la fenetre d'erreur */
    if(!match(buf, "+OK")) {
      printf("%s", buf);
      demandeConnexion = false;
      effacerChamp(&champUser);
      effacerChamp(&champPass);

      champActif = NULL;
      champUser.lenTexte = 0;
      *champUser.texte = '\0';
      champPass.lenTexte = 0;
      *champPass.texte = '\0';

      sprintf(msgErreurLogin, "Echec connexion");

      exposeLogin();

    } else {                    /* Auth réussie */
      /* On passe à la suite */
      connexionReussie = true;
    }

  } while(!connexionReussie);

  fermerFenetreLogin();
  /* Ici l'ordre est important, listerMessages vas allouer les structures msgPop
     mais pas leur champ Window. topMessages effectue les requetes top et mets à
     jour la structure msgPop (toujours sans le champ
     Window). creerFenetreListeMsg ouvre la fenetre principale,
     creerFenetreIdMsg cette fois met à jour le champ Window des msgPop et
     expose affiche les messages. Chaque fonction s'appuie sur des allocations
     faites par les fonctions précédentes */
  listerMessages(sock);
  topMessages(sock);
  creerFenetreListeMsg();
  creerFenetresIdMsg();
  exposeListeMsg();


  /* Tant qu'il n'y a pas de clic sur QUIT */
  while(!veutQuitter) {

    XNextEvent(dpy, &e);
    switch(e.type) {
      case ButtonRelease:
        veutQuitter = !pourButtonReleaseListeMsg((XButtonEvent*)(&e), sock);
        break;
      case Expose:
        exposeListeMsg();
        break;
    default:
      break;
    }

  }

  /* Envoi de la commande QUIT et se termine */
  if(fwrite("QUIT\n", sizeof(char), strlen("QUIT\n"), sock) == -1) {
    perror("Erreur écriture QUIT\n");
    return;
  }
  if(!fgets(buf, sizeof(char)*SIZE_BUFFER, sock)) {
    perror("fgets ");
    return;
  }
  if(!match(buf, "+OK")) {
    printf("%s\n", buf);
    return;
  }
}

